import { ethers } from "hardhat";

async function main() {
  const startMessage : string = "Developed by Abdullatif Wakkaf";
  const con3X = await ethers.deployContract("Con3X", [startMessage]);
  await con3X.waitForDeployment();
  console.log('Con3X Contract deployed with message >> ', startMessage);

}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});
