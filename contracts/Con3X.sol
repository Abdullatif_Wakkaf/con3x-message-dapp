pragma solidity ^0.8.24;


import "hardhat/console.sol";

contract Con3X {
    string private message;

    constructor(string memory _message){
        console.log("Deploying Con3X Contract with a message >> ", _message);
        message = _message;
    }

    function getMessage() public view returns(string memory){
        return message;
    }

    function setMessage(string memory _message) public {
        console.log("Changing the Con3X message from %s to %s", message, _message);
        message = _message;
    }
}