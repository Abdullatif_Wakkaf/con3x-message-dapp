Please refer to this file to see all the work done step by step:
[Project Tasks Step by Step](https://drive.google.com/file/d/1IiHwvFSWkvXXt9YVmPCzLrtTirZVHvHU/view?usp=sharing)

https://drive.google.com/file/d/1IiHwvFSWkvXXt9YVmPCzLrtTirZVHvHU/view?usp=sharing

## How to run the project:
Clone this project to your machine and open it with your code editor (ex: visual studio code) :

Open a new terminal in your project, and type the following commands :
1. install packages : 
```npm install``` 
2. run hardhat node : 
```npx hardhat node```
this will give you some testing accounts that you can use them later in the webpage
3. in a new terminal, run the contract on hardhat node : 
```npx hardhat run .\scripts\deploy.ts --network localhost```
4. run the server : 
```npm run dev```

Now you are ready !

- Open your browser to this url : http://localhost:3000
- Open metamask plugin and add an account from the hardhat node terminal
- Press Get message to get  the default message from the blockchain.
- Enter a new message in the input box and press Set Message button to save the message to the blockchain.
- Confirm the transaction with metamask and the new message wil be displayed in the green box.

# Run Tests
In terminal run this command :
```npx hardhat test```

# Tools and Documentation
This project uses :
- Next.js : see the setup documentation [Next.js](https://nextjs.org/docs/getting-started/installation)
- Hardhat : see the setup documentation [Hardhat](https://hardhat.org/hardhat-runner/docs/getting-started#quick-start)
- Hardhat Ethers Plugin : see the documentaion [hardhat-ethers](https://hardhat.org/hardhat-runner/plugins/nomicfoundation-hardhat-ethers)
- React : see the documentation [React](https://react.dev/learn)
- TypeScript : see the documentation [TypeScript](https://www.typescriptlang.org/docs/handbook/2/basic-types.html)
- Solidity : see the documentation [Soldity](https://docs.soliditylang.org/en/v0.8.25/introduction-to-smart-contracts.html)
- Chai : see the documentation [Chai](https://www.chaijs.com/api/)

