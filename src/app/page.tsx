'use client'

import { ethers } from "ethers";
import { useState } from "react";
import Con3x from "../artifacts/contracts/Con3X.sol/Con3X.json";


const contractAddress : string = '0x5fbdb2315678afecb367f032d93f642f64180aa3';


export default function Con3xMessageDapp(){
    
    const [inputMessage, setInputMessage] = useState("");
    const [message, setMessage] = useState("");


    async function requestAccount() : Promise<void> {
        await window.ethereum.request({method: 'eth_requestAccounts'});
        console.log(window.ethereum);
    }
    
    async function fetchCon3XMessage() : Promise<void> {
        if(typeof window.ethereum !== 'undefined'){
            await requestAccount();
            const provider = new ethers.BrowserProvider(window.ethereum);
            const contract = new ethers.Contract(contractAddress, Con3x.abi, provider);
            try {
                const data : string = await contract.getMessage();
                console.log('data >> ', data);
                setMessage(data);
            } catch (error) {
                console.log('error >>> ', error);
            }
        }
    }

    async function setCon3XMessage() : Promise<void> {
        if(!inputMessage) return;

        if(typeof window.ethereum !== 'undefined'){
            await requestAccount();
            const provider = new ethers.BrowserProvider(window.ethereum);
            const signer = await provider.getSigner();
            const contract = new ethers.Contract(contractAddress, Con3x.abi, signer);
            // console.log(message);
            const transaction = await contract.setMessage(inputMessage);

            setInputMessage("");
            await transaction.wait();
            fetchCon3XMessage();
        }
    }



    return (
      <div className="lg:flex justify-center">
          <div className="lg:w-9/12 xl:w-7/12">
              <header className="m-8 p-2 my-24 flex justify-center text-5xl text-center font-bold bg-clip-text text-transparent bg-gradient-to-r from-pink-500 to-violet-500">
                  Con3X Message Dapp
              </header>

              <div className="m-8 p-4 flex flex-col justify-center bg-yellow-400 rounded-lg">
                  <div className="m-2 flex justify-center">
                      <button className="p-2 rounded-lg bg-indigo-400 ring-2 ring-indigo-600"
                          onClick={fetchCon3XMessage}>
                          Get Message
                      </button>
                  </div>
                  
                  <div className="m-2 p-4 flex flex-col md:flex-row justify-center bg-indigo-500 rounded-lg">
                      <input className="mb-4 md:mb-0 md:mr-4 p-2 text-center rounded text-black"
                          placeholder='Enter a Message'
                          onChange={(e) => setInputMessage(e.target.value)}
                          value={inputMessage}
                      />


                      <div className="flex justify-center">
                          <button className="p-2 max-w-1/2 rounded-lg bg-orange-500 ring-2 ring-orange-300"
                              onClick={setCon3XMessage}>
                              Set Message
                          </button>
                      </div>
                  </div>
              </div>
              
              
              <div className="m-8 p-4 flex flex-col justify-center bg-green-400 rounded-lg">
                  <h1 className="flex justify-center font-bold text-green-800 text-3xl">
                      { (message == "") ? "Message" : message  }
                  </h1>
              </div>
          </div>
      </div>
    );
}