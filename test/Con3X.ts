import { loadFixture } from "@nomicfoundation/hardhat-toolbox/network-helpers";
import { expect } from "chai";
import { ethers } from "hardhat";

describe("Con3X", function () {
    async function deployOneYearLockFixture() {
        // Contracts are deployed using the first signer/account by default
        const [owner, otherAccount] = await ethers.getSigners();
        
        const message = "Con3X message Testing";
        const Con3XFactory = await ethers.getContractFactory("Con3X");
        const con3x = await Con3XFactory.deploy(message);

        return { con3x, message, owner, otherAccount };
    }

    describe("Deployment", function () {        
        it("Should set the correct start message when deploy", async function () {
            const {con3x, message} = await loadFixture(deployOneYearLockFixture);
            expect(await con3x.getMessage()).to.equal(message);
        });

        it("Should change the message correctly", async function () {
            const {con3x} = await loadFixture(deployOneYearLockFixture);
            const newMessage = "this is a new message";
            const transaction = await con3x.setMessage(newMessage);
            await transaction.wait();
            expect(await con3x.getMessage()).to.equal(newMessage);
        });
    });
});
  